﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using TechTalk.SpecFlow.Infrastructure;

namespace SpecFlowProjectAutomationExercise.Drivers
{
    /// <summary>
    /// Manages a browser instance using Selenium
    /// </summary>
    public class BrowserDriver : IDisposable
    {
        private readonly ISpecFlowOutputHelper _specFlowOutputHelper;
        private readonly Lazy<IWebDriver> _currentWebDriverLazy;
        private bool _isDisposed;

        public BrowserDriver(ISpecFlowOutputHelper specFlowOutputHelper)
        {
            _specFlowOutputHelper = specFlowOutputHelper;
            _currentWebDriverLazy = new Lazy<IWebDriver>(CreateWebDriver);
        }

        /// <summary>
        /// The Selenium IWebDriver instance
        /// </summary>
        public IWebDriver Current => _currentWebDriverLazy.Value;

        /// <summary>
        /// Creates the Selenium web driver (opens a browser)
        /// </summary>
        /// <returns></returns>
        ///
        private IWebDriver CreateWebDriver()
        {
            //We use the Chrome browser
            var chromeDriverService = ChromeDriverService.CreateDefaultService();

            var chromeOptions = new ChromeOptions();
         
            //local
            var chromeDriver = new ChromeDriver(chromeDriverService, chromeOptions);

            //Remote
            //var chromeDriver = new RemoteWebDriver(new Uri("http://localhost:4444"), chromeOptions);
            _specFlowOutputHelper?.WriteLine("Browser launched");

            // chromeDriver = new RemoteWebDriver(http://localhost:9999, chromeOptions);

            return chromeDriver;
        }

        /*   private IWebDriver CreateWebDriver()
           {
               //We use the firefox browser
               var fireFoxDriverService = FirefoxDriverService.CreateDefaultService();

               var firefoxOptions = new FirefoxOptions();

               var fireFoxDriver = new FirefoxDriver(fireFoxDriverService, firefoxOptions);

               return fireFoxDriver;
           }*/

        /// <summary>
        /// Disposes the Selenium web driver (closing the browser)
        /// </summary>
        public void Dispose()
        {
            if (_isDisposed)
            {
                return;
            }

            if (_currentWebDriverLazy.IsValueCreated)
            {
                Current.Quit();
                _specFlowOutputHelper?.WriteLine("Browser closed");
            }

            _isDisposed = true;
        }
    }
}