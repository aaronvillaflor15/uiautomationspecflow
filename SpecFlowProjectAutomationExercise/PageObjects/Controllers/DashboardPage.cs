﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecFlowProjectAutomationExercise.PageObjects.Locators;
using FluentAssertions;
using SeleniumExtras.WaitHelpers;
using System;

namespace SpecFlowProjectAutomationExercise.PageObjects.Controllers

{
    public class DashboardPage

    {
        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;

        public DashboardPage(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void IsDashboardVisible()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(DashboardPageLocators.Header.Dashboard));
        }
    }
}