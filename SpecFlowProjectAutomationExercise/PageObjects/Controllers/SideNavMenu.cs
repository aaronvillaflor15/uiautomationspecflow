﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SpecFlowProjectAutomationExercise.PageObjects.Locators;
using FluentAssertions;
using SeleniumExtras.WaitHelpers;
using System;

namespace SpecFlowProjectAutomationExercise.PageObjects.Controllers

{
    public class SideNavMenu

    {
        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;

        public SideNavMenu(IWebDriver webDriver)
        {
            _webDriver = webDriver;
        }

        public void ClickPIM()
        {
            _webDriver.FindElement(SideNavMenuLocators.NavMenu.PIM).Click();
        }
    }
}