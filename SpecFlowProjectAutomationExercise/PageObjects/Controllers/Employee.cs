﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using SpecFlowProjectAutomationExercise.PageObjects.Locators;

namespace SpecFlowProjectAutomationExercise.PageObjects.Controllers

{
    public class Employee

    {
        //The Selenium web driver to automate the browser
        private readonly IWebDriver _webDriver;

        private string BorderValue = "1px solid rgb(235, 9, 16)";
        private string firstName = Faker.Name.First();
        private string lastName = Faker.Name.Last();
        private int number = Faker.RandomNumber.Next(1000);

        public Employee(IWebDriver webDriver)
        {
            _webDriver = webDriver;
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
        }

        public void AttachFile()
        {
            /*Console.WriteLine("Current directory path: " +
                    Directory.GetCurrentDirectory());
            var upload = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.Browse);
            upload.Click();
            // using "HandleOpenDialog" to locate and upload file
            HandleOpenDialog hndOpen = new HandleOpenDialog();
            hndOpen.fileOpenDialog(@"\SpecFlowProjectAutomationExercise\SpecFlowProjectAutomationExercise\Assets\image", "hitsu.png");
*/

            //  HandleOpenDialog hndOpen = new HandleOpenDialog();
            string baseDirectory = AppContext.BaseDirectory;
            string relativePath = "../../../Assets/image/testautomation_test.png";
            string uploadFile = Path.GetFullPath(Path.Combine(baseDirectory, relativePath));

            var saveBtn = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.Save);
            var upload = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.Browse);
            new Actions(_webDriver)
              .ScrollToElement(saveBtn)
              .Perform();
            upload.SendKeys(uploadFile);
        }

        public void CheckMandatoryFields()
        {
            string LastNameBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.LastName).GetCssValue("border");
            string UsernameBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.UserName).GetCssValue("border");
            string PasswordBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.Password).GetCssValue("border");
            string ConfirmPasswordBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.ConfirmPassword).GetCssValue("border");

            LastNameBorderValue.Should().BeEquivalentTo(BorderValue);
            UsernameBorderValue.Should().BeEquivalentTo(BorderValue);
            PasswordBorderValue.Should().BeEquivalentTo(BorderValue);
            ConfirmPasswordBorderValue.Should().BeEquivalentTo(BorderValue);
        }

        public void ClickAddAttachment()
        {
            //Explicit wait
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.PersonalDetails.Button.AddAttachment));

            var element = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.AddAttachment);
            Actions actions = new Actions(_webDriver);
            actions.MoveToElement(element);
            actions.Perform();
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.AddAttachment).Click();
        }

        public void ClickAddButton()
        {
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Button.Add).Click();
        }

        public void ClickDescending()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.SortButton));
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Table.SortButton).Click();

            var element = _webDriver.FindElement(EmployeePageLocators.EmployeeList.Table.Descending);
            Actions actions = new Actions(_webDriver);
            actions.MoveToElement(element);
            actions.Perform();
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Table.Descending).Click();
        }

        public void ClickEmployeeList()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.TopMenu.EmployeeList).Click();
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.EmployeeTable)).Displayed);
        }

        public void ClickResetButton()
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Button.Reset).Click();
        }

        public void ClickSaveButton()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));

            Assert.IsTrue(wait.Until(ExpectedConditions.ElementToBeClickable(EmployeePageLocators.AddEmployeee.Button.Save)).Enabled);

            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.Button.Save).Click();
        }

        public void ClickSaveButtonAttachment()
        {
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Button.Save).Click();
        }

        public void ClickSearchButton()
        {
            _webDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Button.Search).Click();
        }

        public void ClickUserInTable()
        {
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.Table.NameMidNameCell).Click();
        }

        public void IsPesonalDetailsDisplayed()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.PersonalDetails.TextField.PersonalDetails)).Displayed);
        }

        public string GetEmployeeId()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.PersonalDetails.TextField.EmployeeId)).Displayed);
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.TextField.EmployeeId).Click();
            string valueTxt = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.TextField.EmployeeId).GetAttribute("value");

            return valueTxt;
        }

        public void IsEmployeePageLoaded()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            Assert.IsTrue(wait.Until(ExpectedConditions.UrlToBe("https://opensource-demo.orangehrmlive.com/web/index.php/pim/viewEmployeeList")));

            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.EmployeeTable)).Displayed);
        }

        public void PopulateAddEmployeeFields()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.LastName).SendKeys(lastName);
            string empId = number.ToString();
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.EmployeeId).SendKeys(empId);
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.UserName).SendKeys("username" + number);
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.Password).SendKeys("P@ssw0rd123!");
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.AddEmployeee.TextField.TextBetter)).Displayed);
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.ConfirmPassword).Click();
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.ConfirmPassword).SendKeys("P@ssw0rd123!");
        }

        public void PopulateAddEmployeeNameField()
        {
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.FirstName).SendKeys(firstName);
        }

        public void SearchForEmployeeId(string empId)
        {
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.TextField.EmployeeId).Click();
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.TextField.EmployeeId).Clear();
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.TextField.EmployeeId).SendKeys(empId);
        }

        public void SearchForEmployeeName()
        {
            _webDriver.FindElement(EmployeePageLocators.EmployeeList.TextField.EmployeeName).SendKeys("Sara");
        }

        public void SuccessfulSnackBarDisplayed()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.AddEmployeee.SnackBar.SuccessfulSnackBar)).Displayed);
        }

        public void ToggleCreateLoginDetails()
        {
            _webDriver.FindElement(EmployeePageLocators.AddEmployeee.ToggleButton.CreateLoginDetails).Click();
        }

        public string ValidateEmployeeInTable()
        {
            Thread.Sleep(2000);
            //  WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            // Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.EmployeeTable)).Displayed);

            string employeeIdText = _webDriver.FindElement(EmployeePageLocators.EmployeeList.Table.EmployeeIdCell).Text;

            return employeeIdText;
        }

        public void IsEmployeeTableDisplayed()
        {
            Thread.Sleep(2000);
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.EmployeeTable)).Displayed);
        }

        public void ValidateErrorsAreGone()
        {
            string LastNameBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.LastName).GetCssValue("border");
            LastNameBorderValue.Should().NotBeEquivalentTo(BorderValue);
            string UsernameBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.UserName).GetCssValue("border");
            UsernameBorderValue.Should().NotBeEquivalentTo(BorderValue);
            string ConfirmPasswordBorderValue = _webDriver.FindElement(EmployeePageLocators.AddEmployeee.TextField.ConfirmPassword).GetCssValue("border");
            ConfirmPasswordBorderValue.Should().NotBeEquivalentTo(BorderValue);
        }

        public void VerifyAttachmentInTable2()
        {
            string imgName = "testautomation_test.png";
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.PersonalDetails.Table.AttachmentTable));

            string valueTxt = _webDriver.FindElement(EmployeePageLocators.EmployeeList.PersonalDetails.Table.FileName).Text;

            Console.WriteLine(valueTxt);
            valueTxt.Should().BeEquivalentTo(imgName);
        }

        public void VerifyAttachmentInTable()
        {
            string testFileNamee = "testautomation_test.png";
            // grab the cells that contain the display names you want to verify are sorted
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.PersonalDetails.Table.AttachmentTable));
            var fileNameCells = _webDriver.FindElements(EmployeePageLocators.EmployeeList.PersonalDetails.Table.FileName);

            // loop through the cells and assign the display names into the ArrayList
            foreach (IWebElement fileName in fileNameCells)
            {
                if (fileName.Text == testFileNamee)
                {

                    Console.WriteLine(fileName);
                    testFileNamee.Should().BeEquivalentTo(testFileNamee);
                    //Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(fileName{})).Displayed);
                   // Assert.IsTrue(fileName().dI;
                }
                else

                    testFileNamee.Should().NotBeEquivalentTo(testFileNamee);
            }
        }

        public void VerifyFirstAndMiddleNameInAscendingOrder()
        {
            Thread.Sleep(2000);
            //WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            // Assert.IsTrue(wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.EmployeeTable)).Displayed);
            List<String> displayNames = new List<string>();

            // grab the cells that contain the display names you want to verify are sorted
            var cells = _webDriver.FindElements(EmployeePageLocators.EmployeeList.Table.NameMidNameCell);

            // loop through the cells and assign the display names into the ArrayList
            foreach (IWebElement cell in cells)
            {
                displayNames.Add(cell.Text);
                Console.WriteLine("Orig Firstname Middlenam: " + cell.Text);
            }

            // make a copy of the displayNames array
            List<String> displayNamesSorted = new List<string>(displayNames);
            displayNamesSorted.Sort();
            foreach (String displayName in displayNamesSorted)
            {
                Console.WriteLine("Sorted: " + displayName);
            }

            Console.WriteLine(displayNames.SequenceEqual(displayNamesSorted));

            displayNames.Should().BeEquivalentTo(displayNamesSorted);
            //Assert.IsTrue(displayNames.SequenceEqual(displayNamesSorted));
        }

        public void VerifyLastNameInDescendingOrder()
        {
            WebDriverWait wait = new WebDriverWait(_webDriver, TimeSpan.FromSeconds(30));
            wait.Until(ExpectedConditions.ElementIsVisible(EmployeePageLocators.EmployeeList.Table.LasNameCell));
            List<String> displayNames = new List<string>();

            // grab the cells that contain the display names you want to verify are sorted
            var cells = _webDriver.FindElements(EmployeePageLocators.EmployeeList.Table.LasNameCell);

            // loop through the cells and assign the display names into the ArrayList
            foreach (IWebElement cell in cells)
            {
                displayNames.Add(cell.Text);
                //Console.WriteLine("Orig Lastname: " + cell.Text);
            }

            // make a copy of the displayNames array
            List<String> displayNamesSorted = new List<string>(displayNames);

            foreach (String displayName in displayNamesSorted)
            {
                // Console.WriteLine("Reversed Lastname: " + displayName);
            }

            Console.WriteLine(displayNames.SequenceEqual(displayNamesSorted));
            displayNames.Should().BeEquivalentTo(displayNamesSorted);
            // Assert.IsTrue(displayNames.SequenceEqual(displayNamesSorted));
        }
    }
}