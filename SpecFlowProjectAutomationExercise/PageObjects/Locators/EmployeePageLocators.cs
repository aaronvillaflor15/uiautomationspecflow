﻿using OpenQA.Selenium;

namespace SpecFlowProjectAutomationExercise.PageObjects.Locators
{
    public static class EmployeePageLocators
    {
        public static class AddEmployeee
        {
            public static class Button
            {
                public static By Save => By.XPath("//button[@type='submit'][contains(.,'Save')]");
            }

            public static class SnackBar
            {
                public static By SuccessfulSnackBar => By.XPath("//div[@class='oxd-toast oxd-toast--success oxd-toast-container--toast']");
            }

            public static class TextField
            {
                public static By ConfirmPassword => By.XPath("(//input[@type='password'])[2]");
                public static By EmployeeId => By.CssSelector("#app > div.oxd-layout > div.oxd-layout-container > div.oxd-layout-context > div > div > form > div.orangehrm-employee-container > div.orangehrm-employee-form > div:nth-child(1) > div.oxd-grid-2.orangehrm-full-width-grid > div > div > div:nth-child(2) > input");
                public static By EmployeeIdTxt => By.XPath("//label[@class='oxd-label'][contains(.,'Employee Id')]");
                public static By FirstName => By.Name("firstName");
                public static By LastName => By.Name("lastName");
                public static By Password => By.XPath("(//input[@type='password'])[1]");
                public static By UserName => By.XPath("(//input[@autocomplete='off'])[1]");
                public static By TextBetter => By.XPath("//span[contains(.,'Better')]");
            }

            public static class ToggleButton
            {
                public static By CreateLoginDetails => By.CssSelector(".oxd-switch-input.oxd-switch-input--active.--label-right");
            }
        }

        public static class EmployeeList
        {
            public static class Button
            {
                public static By Add => By.CssSelector("button[class='oxd-button oxd-button--medium oxd-button--secondary']");
                public static By Reset => By.XPath("//button[@type='reset'][contains(.,'Reset')]");
                public static By Search => By.XPath("//button[@type='submit'][contains(.,'Search')]");
            }

            public static class PersonalDetails
            {
                public static class Button
                {
                    public static By AddAttachment => By.XPath("//button[@type='button'][contains(.,'Add')]");
                    public static By Browse => By.CssSelector("input[type=file]");
                    public static By Save => By.CssSelector("div[class='orangehrm-attachment'] button[type='submit']");
                }

                public static class Table
                {
                    public static By FileName => By.CssSelector(".oxd-table-cell.oxd-padding-cell:nth-child(2)");
                    public static By AttachmentTable => By.CssSelector(".oxd-table-body");
                }

                public static class TextField
                {
                    public static By EmployeeId => By.XPath("(//input[@class='oxd-input oxd-input--active'])[3]");
                    public static By PersonalDetails => By.XPath("//h6[contains(.,'Personal Details')]");
                    public static By LoadingSpinner => By.XPath("//div[@class='oxd-loading-spinner']");
                }
            }

            public static class Table
            {
                public static By EmployeeTable => By.CssSelector(".orangehrm-container");
                public static By Descending => By.XPath("(//span[contains(.,'Descending')])[3]");
                public static By EmployeeIdCell => By.CssSelector(".oxd-table-cell.oxd-padding-cell:nth-child(2)");
                public static By LasNameCell => By.CssSelector(".oxd-table-cell.oxd-padding-cell:nth-child(4)");
                public static By NameMidNameCell => By.CssSelector(".oxd-table-cell.oxd-padding-cell:nth-child(3)");
                public static By SortButton => By.XPath("(//i[@class='oxd-icon bi-arrow-down-up oxd-icon-button__icon oxd-table-header-sort-icon'])[2]");
            }

            public static class TextField
            {
                public static By EmployeeId => By.CssSelector("#app > div.oxd-layout > div.oxd-layout-container > div.oxd-layout-context > div > div.oxd-table-filter > div.oxd-table-filter-area > form > div.oxd-form-row > div > div:nth-child(2) > div > div:nth-child(2) > input");
                public static By EmployeeIdClicked => By.XPath("//input[contains(@class,'oxd-input oxd-input--focus')]");
                public static By EmployeeName => By.XPath("(//input[@placeholder='Type for hints...'])[1]");
            }

            public static class TopMenu
            {
                public static By EmployeeList => By.XPath("//a[contains(.,'Employee List')]");
            }
        }
    }
}