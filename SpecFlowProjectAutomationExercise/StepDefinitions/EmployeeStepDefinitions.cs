using NUnit.Framework;
using SpecFlowProjectAutomationExercise.Drivers;
using SpecFlowProjectAutomationExercise.Hooks;
using SpecFlowProjectAutomationExercise.PageObjects.Controllers;

namespace SpecFlowProjectAutomationExercise.StepDefinitions
{
    [TestFixture]
    [Binding]
    public class EmployeeStepDefinitions
    {
        private readonly LoginPage _loginPage;

        private readonly SideNavMenu _sideNavMenu;
        private readonly Employee _employee;

        public EmployeeStepDefinitions(BrowserDriver browserDriver)
        {
            _loginPage = new LoginPage(browserDriver.Current);
            _sideNavMenu = new SideNavMenu(browserDriver.Current);
            _employee = new Employee(browserDriver.Current);
        }

        [Given(@"User is logged in as Admin")]
        public void GivenUserIsLoggedInAsAdmin()
        {
            _loginPage.GotoLoginPage();
            _loginPage.LoginUser("Admin", "admin123");
        }

        [Given(@"User clicks PIM in side menu bar")]
        public void GivenUserClicksPIMInSideMenuBar()
        {
            _sideNavMenu.ClickPIM();
        }

        [When(@"User clicks Employee List in the top menu")]
        public void WhenUserClicksEmployeeListInTheTopMenu()
        {
            _employee.ClickEmployeeList();
        }

        [Then(@"Employee List page is loaded")]
        public void ThenEmployeeListPageIsLoaded()
        {
            _employee.IsEmployeePageLoaded();
        }

        [When(@"User clicks Add Employee button")]
        public void WhenUserClicksAddEmployeeButton()
        {
            _employee.ClickAddButton();
        }

        [When(@"User enter Employee's name")]
        public void WhenUserEnterEmployeesName()
        {
            _employee.PopulateAddEmployeeNameField();
        }

        [When(@"User clicks Create Login Details")]
        public void WhenUserClicksCreateLoginDetails()
        {
            _employee.ToggleCreateLoginDetails();
        }

        [When(@"User clicks Save button")]
        public void WhenUserClicksSaveButton()
        {
            _employee.ClickSaveButton();
        }

        [When(@"User validate errors")]
        public void WhenUserValidateErrors()
        {
            _employee.CheckMandatoryFields();
        }

        [When(@"User populate the mandatory fields")]
        public void WhenUserPopulateTheMandatoryFields()
        {
            _employee.PopulateAddEmployeeFields();
        }

        [When(@"User validate that errors are gone")]
        public void WhenUserValidateThatErrorsAreGone()
        {
            _employee.ValidateErrorsAreGone();
        }

        [When(@"User clicks Save")]
        public void WhenUserClicksSave()
        {
            _employee.ClickSaveButton();
        }

        [Then(@"Successful snack bar should be displayed")]
        public void ThenSuccessfulSnackBardShouldBeDisplayed()
        {
            _employee.SuccessfulSnackBarDisplayed();
        }

        [Then(@"Employee is added to the list of employees")]
        public void ThenEmployeeIsAddedToTheListOfEmployees()
        {
            _employee.IsPesonalDetailsDisplayed();
            _employee.GetEmployeeId();
            string empId = _employee.GetEmployeeId();
            _employee.ClickEmployeeList();
            _employee.SearchForEmployeeId(empId);
            _employee.ClickSearchButton();
            _employee.IsEmployeeTableDisplayed();
            _employee.ValidateEmployeeInTable();
            string employeeIdcell = _employee.ValidateEmployeeInTable();
            empId.Should().BeEquivalentTo(employeeIdcell);
        }

        [When(@"User clicks reset button")]
        public void WhenUserClicksResetButton()
        {
            _employee.ClickResetButton();
        }

        [When(@"Verify that Firstname and Middle name is in Ascending order")]
        public void WhenVerifyThatFirstnameAndMiddleNameIsInAscendingOrder()
        {
            _employee.VerifyFirstAndMiddleNameInAscendingOrder();
        }

        [When(@"User clicks descending order in Last name")]
        public void WhenUserClicksDescendingOrderInLastName()
        {
            _employee.ClickDescending();
        }

        [Then(@"Lastname column should be in descending order")]
        public void ThenLastnameColumnShouldBeInDescendingOrder()
        {
            _employee.VerifyLastNameInDescendingOrder();
        }

        [When(@"User Enters the employee's name")]
        public void WhenUserEntersTheEmployeesName()
        {
            _employee.SearchForEmployeeName();
        }

        [When(@"Click search button")]
        public void WhenClickSearchButton()
        {
            _employee.ClickSearchButton();
        }

        [When(@"User click the employee in the table")]
        public void WhenUserClickTheEmployeeInTheTable()
        {
            _employee.ClickUserInTable();
        }

        [Then(@"User verifies the selected employee")]
        public void ThenUserVerifiesTheSelectedEmployee()
        {
            Console.WriteLine("Verify employee");
        }

        [When(@"User clicks add button in the attachments")]
        public void WhenUserClicksAddButtonInTheAttachments()
        {
            _employee.ClickAddAttachment();
        }

        [When(@"Clicks browse button and attached file")]
        public void WhenClicksBrowseButtonAndAttachedFile()
        {
            _employee.AttachFile();
        }

        [When(@"Clicks save button")]
        public void WhenClicksSaveButton()
        {
            _employee.ClickSaveButtonAttachment();
        }

        [Then(@"User verifies the file name attached in the table")]
        public void ThenUserVerifiesTheFileNameAttachedInTheTable()
        {
            _employee.VerifyAttachmentInTable();
        }

        [When(@"User delete the attached file")]
        public void WhenUserDeleteTheAttachedFile()
        {
        }

        [Then(@"User verifies thee name is not displayed in the table")]
        public void ThenUserVerifiesTheeNameIsNotDisplayedInTheTable()
        {
        }
    }
}