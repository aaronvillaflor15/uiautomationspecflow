using NUnit.Framework;
using SpecFlowProjectAutomationExercise.Drivers;
using SpecFlowProjectAutomationExercise.Hooks;
using SpecFlowProjectAutomationExercise.PageObjects.Controllers;
using System;
using TechTalk.SpecFlow;

namespace SpecFlowProjectAutomationExercise.StepDefinitions
{
    [TestFixture]
    [Binding]
    public class OrangeHRMLoginStepDefinitions
    {
        private readonly LoginPage _loginPage;
        private readonly DashboardPage _dashboardPage;
        private readonly SideNavMenu _sideNavMenu;

        public OrangeHRMLoginStepDefinitions(BrowserDriver browserDriver)
        {
            _loginPage = new LoginPage(browserDriver.Current);
            _dashboardPage = new DashboardPage(browserDriver.Current);
            _sideNavMenu = new SideNavMenu(browserDriver.Current);
        }
    
        [Given(@"User is on the login page")]
        public void GivenUserIsOnTheLoginPage()
        {
            _loginPage.GotoLoginPage();
        }

        [When(@"User enters ""([^""]*)"" as Username")]
        public void WhenUserEntersAsUsername(string admin)
        {
            _loginPage.EnterUsername("Admin");
        }

        [When(@"User enters ""([^""]*)"" as Password")]
        public void WhenUserEntersAsPassword(string p0)
        {
            _loginPage.EnterPassword("admin123");
        }

        [When(@"User clicks on the login button")]
        public void WhenUserClicksOnTheLoginButton()
        {
            _loginPage.ClickLoginButton();
        }

        [Then(@"Login should be successful")]
        public void ThenLoginShouldBeSuccessful()
        {
            _dashboardPage.IsDashboardVisible();
        }
    }
}