﻿
Feature: OrangeHRM Login

Scenario: Login with valid credentials
	Given User is on the login page
	When User enters "Admin" as Username
	And User enters "admin123" as Password
	And User clicks on the login button
	Then Login should be successful


