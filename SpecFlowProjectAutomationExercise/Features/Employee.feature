﻿Feature: Employee List

Background: 
Given User is logged in as Admin
And User clicks PIM in side menu bar


Scenario: Navigate to Employee List
	When User clicks Employee List in the top menu
	Then Employee List page is loaded


Scenario: Add New Employee
	When User clicks Add Employee button
	And User enter Employee's name
	And User clicks Create Login Details
	And User clicks Save button
	And User validate errors
	And User populate the mandatory fields
	And User validate that errors are gone
	And User clicks Save 
	Then Successful snack bar should be displayed
	And Employee is added to the list of employees


Scenario: Verify employee details table can be sorted by name
	When User clicks reset button
	And Verify that Firstname and Middle name is in Ascending order
	And User clicks descending order in Last name
	Then Lastname column should be in descending order


Scenario: Verify attachments can be added in the Personal Details page
	When User Enters the employee's name
	And Click search button 
	And User click the employee in the table
	Then User verifies the selected employee
	When User clicks add button in the attachments
	And Clicks browse button and attached file
	And Clicks save button
	Then User verifies the file name attached in the table
	When User delete the attached file
	Then User verifies thee name is not displayed in the table
	